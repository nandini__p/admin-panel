const express = require("express");

const PORT = process.env.PORT || 3001;

const app = express();
const cors = require('cors')

const userRoute = require('./routes/user')
const verifyUser = require('./routes/verifyUser')
const connectDb = require('./db/connect');
const { authUser} = require("./middleware/authorization");

app.use(express.json())
app.use(cors())
app.use('/api/v1/user',userRoute)
app.use('/verifyRole',authUser(['admin']),verifyUser)

const start = async()=>{
  try{
    await connectDb()
    app.listen(PORT, () => {
      console.log(`Server listening on ${PORT}`);
    });
  }catch(e){
    console.log(e)
  }
}

start()