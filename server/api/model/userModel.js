require('dotenv').config()
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true,'Please provide a name']
    },
    email:{
        type:String,
        required:[true,'Please provide a email'],
        unique:true
    },
    password:{
        type:String,
        require:[true,'Please provide a password'],
        minlength:6
    },
    role:{
        type:String
    }
})

userSchema.pre('save', async function(next){
    const salt= await bcrypt.genSalt(10)
    this.password =await bcrypt.hash(this.password,salt)
    next()
})

userSchema.methods.createJWT = function(){
    return jwt.sign({
        userId : this._id,
        role:this.role
    },
    process.env.JWT_SECRET,
    {
        expiresIn: process.env.JWT_LIFETIME
    }
    )
    
   
}

userSchema.methods.comparePasswords = async function(candidatePass){
    return ismatch = await bcrypt.compare(candidatePass,this.password)
}

module.exports = mongoose.model('users',userSchema)
