require('dotenv').config()
const jwt = require('jsonwebtoken')

const authUser = (permissions) => {
    return (req, res, next) => {
        const authHeader = req.headers.authorization
        if (!authHeader || !authHeader.startsWith('Bearer')) {
            res.status(401).json("Authentication failed")
        }
        else {
            const token = authHeader.split(' ')[1]
            const payload = jwt.verify(token, process.env.JWT_SECRET)
            req.user = {
                userId: payload.userId,
                role: payload.role
            }

            const userRole = req.user.role
            console.log(userRole)


            if (permissions.includes(userRole)) {
                next()
            } else {
                return res.status(401).json('Unauthorised access')
            }
        }

    }

}

const auth = (req, res, next) => {
    const authHeader = req.headers.authorization
    if (!authHeader || !authHeader.startsWith('Bearer')) {
        res.status(401).json("Authentication failed")
    }
    else {
        const token = authHeader.split(' ')[1]
        try {
            const payload = jwt.verify(token, process.env.JWT_SECRET)
            req.user = {
                userId: payload.userId,
                role: payload.role
            }
            next()
        }
        catch (e) {
            console.error(e)
        }
    }
    return req.user.role
}

module.exports = {
    authUser,
    auth
}