const mongoose= require('mongoose')

const connectDb =async ()=>{
    try {
        await mongoose.connect('mongodb://127.0.0.1:27017/react-admin-db')
        return console.log('MongoDB connected sucessfully')
    } catch (error) {
        return console.log(error)
    }
}

module.exports = connectDb