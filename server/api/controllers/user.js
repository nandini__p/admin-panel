require('dotenv').config()
const users = require('../model/userModel')

const register = async (req, res) => {
    try {
        const { name, email, password, role } = req.body
        if(role === 'admin'){
            const user = await users.findOne({role})
            if(user){
                res.status(409).json('Admin already exists')
            }  
            else{
                const user = await users.create({
                    name: name,
                    email: email,
                    password: password,
                    role: role
                })
                const token = user.createJWT()
                res.status(201).json({
                    user:{
                        name:user.name
                    },
                    token
                })
            }  
        }
        else{
            const user = await users.create({
                name: name,
                email: email,
                password: password,
                role: role
            })
            const token = user.createJWT()
            res.status(201).json({
                user:{
                    name:user.name
                },
                token
            })
        }
    } catch (e) {
        console.error(e)
    }
}

const login = async (req, res) => {
    try {
        const { email, password } = req.body
        if (!email || !password) {
            res.status(400).send('Please provide email and password')
        }
        else {
            const user = await users.findOne({
                email
            })

            const IspasswordCorrect = await user.comparePasswords(password)

            if (!user || !IspasswordCorrect) {
                res.status(401).send('Invalid credentials')
            }

            const token = user.createJWT()
            await users.findByIdAndUpdate(user._id,{token})
            
            res.status(200).json({
                user: {
                    name: user.email,
                },
                token
            })
        }

    }
    catch (e) {
        console.error(e)
    }
}

module.exports = {
    register,
    login
}