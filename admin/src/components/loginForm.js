import { useState } from "react";
import '../css/style.css'
import axios from 'axios'
import {useNavigate} from 'react-router-dom'

function LoginForm(){
    const [details,setDetails]=useState({
        name:"",
        pass:"" 
    })
    const [errormsg,setErrormsg] = useState("")

    const navigate = useNavigate()

    function handleChange(e){
        const {name,value}=e.target;

        setDetails((prevDetails)=>{
            return{
                ...prevDetails,
                [name]:value
            }
        })
    }
   
    function SubmitDetails(e){
        // console.log('state',details.name ,details.pass)
        if(details.name=== "" || details.pass===""){
          setErrormsg('Please enter the details !') 
        }
        else{
            axios.post('http://localhost:3001/api/v1/user/login',{
                email:details.name,
                password:details.pass
            })
            .then(res=>{
                console.log(res)
                navigate('/admin/dashboard')   
            })
            .catch(e=>{
                console.log(e)
                if(e.response.status === 401){
                    setErrormsg('Invalid credentials')
                }
            })
        }
        setDetails({
            name:"",
            pass:""
        })
        e.preventDefault();
    }

    
    return (
        <div className="formContainer"> 
        <div className="loginform">
            <form>
                <input type="text" name="name" value={details.name} placeholder="Username" onChange={handleChange}></input><br></br>
                <input type="password" name="pass" value={details.pass} placeholder="Password" onChange={handleChange}></input><br></br>
                <button type="submit" onClick={SubmitDetails}>Log In</button>
                <p className="error">{errormsg}</p>
            </form>
        </div>
        </div>
    );
}

export default LoginForm;