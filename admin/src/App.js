import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import Dashboard from './components/dashboard';
import LoginForm from './components/loginForm';
function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/admin/login" element={<LoginForm/>}/>
        <Route exact path ="/admin/dashboard" element={<Dashboard/>}/>
      </Routes>
    </Router>
  );
}

export default App;
